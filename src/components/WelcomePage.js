import React from 'react';

export default () => (
  <div className="container has-text-centered">
    <h1 className="title is-1">
        Inference-engine
    </h1>
    <h4 className="subtitle is-4">
        Knowledge base about <strong>BOOKS</strong>!
    </h4>
    <h6 className="subtitle is-6">
        by <a href="https://bitbucket.com/lazarchuuk">Victoria Lazarchuk</a>
    </h6>
  </div>
);
